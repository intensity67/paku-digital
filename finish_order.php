<?php session_start(); ?>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<link href="css/bootstrap.min.css" rel="stylesheet">
 	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<?php if(!isset($_SESSION['orders'])) {

	header("Location: 3.php");

} ?>

<body>
<div class="container">
<h2 style="margin-top: 20px;"> Orders Summary  </h2>

<!-- <center> -->

<!-- <table width="700px;"> -->
<table>
	<tr>
		<td>
			<h4> Table No: </h4>
		</td>
		<td>
			<h4><?php echo $_SESSION['tableNumber']; ?></h4>
		</td>
	<tr>
		<td><h4> No of People: </h4></td>
		<td><h4><?php echo $_SESSION['peopleNumber']; ?></h4></td>
	</tr>
</table>







<!-- <table border = 1 width=" 700px;"> -->
<table>

	<tr>
		<td><b> Qty </b></td>
		<td><b> Product Name </b></td>
	</tr>

		<?php for($i = 0; $i < count($_SESSION['orders']['product_name']); $i++){ ?>

			<tr>
				<td> <?php echo $_SESSION['orders']['order_qty'][$i];  ?></td>
				<td> <?php echo $_SESSION['orders']['product_name'][$i];  ?></td>
			</tr>
		<?php } ?>

</table>

<!-- </center> -->

<!-- <center> -->

	<br><br><br>

	<a href="save_order.php" class="btn btn-primary"> Confirm Order </a>

	<br><br> 

	<a href="3.php" class="btn btn-success"> Edit Orders </a>
 
	<br><br> 
	
	<a href="1.php" class="btn btn-info">Back to Order</a>

<!-- </center> -->
</div>
</body>

<script src="js/bootstrap.min.js"></script>

</html>
