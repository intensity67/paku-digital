function openQtyClassic() {
	var cla = document.getElementById('qtyClassic');
	if (cla.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyClassic").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonClassic").css("background-color","initial");
			$("button#buttonClassic").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyClassic").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonClassic").css("background-color","red");
			$("button#buttonClassic").css("color","white");
		});
	}
}
function openQtyBbq() {
	var bbq = document.getElementById('qtyBbq');
	if (bbq.style.display == 'block'){
		$(document).ready(function(){
			 $("#qtyBbq").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonBbq").css("background-color","initial");
			$("button#buttonBbq").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyBbq").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonBbq").css("background-color","red");
			$("button#buttonBbq").css("color","white");
		});
	}
}

function openQtyTeriyaki() {
	var ter = document.getElementById('qtyTeriyaki');
	if (ter.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyTeriyaki").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonTeriyaki").css("background-color","initial");
			$("button#buttonTeriyaki").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyTeriyaki").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonTeriyaki").css("background-color","red");
			$("button#buttonTeriyaki").css("color","white");
		});
	}
}

function openQtyHotwings() {
	var hot = document.getElementById('qtyHotwings');
	if (hot.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyHotwings").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonHotwings").css("background-color","initial");
			$("button#buttonHotwings").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyHotwings").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonHotwings").css("background-color","red");
			$("button#buttonHotwings").css("color","white");
		});
	}
}

function openQtySpicyvinegar() {
	var spi = document.getElementById('qtySpicyvinegar');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtySpicyvinegar").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonSpicyvinegar").css("background-color","initial");
			$("button#buttonSpicyvinegar").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtySpicyvinegar").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonSpicyvinegar").css("background-color","red");
			$("button#buttonSpicyvinegar").css("color","white");
		});
	}
}

function openQtyLemonpepper() {
	var spi = document.getElementById('qtyLemonpepper');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyLemonpepper").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonLemonpepper").css("background-color","initial");
			$("button#buttonLemonpepper").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyLemonpepper").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonLemonpepper").css("background-color","red");
			$("button#buttonLemonpepper").css("color","white");
		});
	}
}

function openQtyCreamcheese() {
	var spi = document.getElementById('qtyCreamcheese');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyCreamcheese").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonCreamcheese").css("background-color","initial");
			$("button#buttonCreamcheese").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyCreamcheese").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonCreamcheese").css("background-color","red");
			$("button#buttonCreamcheese").css("color","white");
		});
	}
}

function openQtyHoneycaramel() {
	var spi = document.getElementById('qtyHoneycaramel');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyHoneycaramel").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonHoneycaramel").css("background-color","initial");
			$("button#buttonHoneycaramel").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyHoneycaramel").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonHoneycaramel").css("background-color","red");
			$("button#buttonHoneycaramel").css("color","white");
		});
	}
}

function openQtyMangosuave() {
	var spi = document.getElementById('qtyMangosuave');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyMangosuave").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonMangosuave").css("background-color","initial");
			$("button#buttonMangosuave").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyMangosuave").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonMangosuave").css("background-color","red");
			$("button#buttonMangosuave").css("color","white");
		});
	}
}

function openQtyBluelemonade() {
	var spi = document.getElementById('qtyBluelemonade');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyBluelemonade").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonBluelemonade").css("background-color","initial");
			$("button#buttonBluelemonade").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyBluelemonade").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonBluelemonade").css("background-color","red");
			$("button#buttonBluelemonade").css("color","white");
		});
	}
}

function openQtyCucumberlemonade() {
	var spi = document.getElementById('qtyCucumberlemonade');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyCucumberlemonade").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonCucumberlemonade").css("background-color","initial");
			$("button#buttonCucumberlemonade").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyCucumberlemonade").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonCucumberlemonade").css("background-color","red");
			$("button#buttonCucumberlemonade").css("color","white");
		});
	}
}

function openQtyRedhorse() {
	var spi = document.getElementById('qtyRedhorse');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyRedhorse").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonRedhorse").css("background-color","initial");
			$("button#buttonRedhorse").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyRedhorse").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonRedhorse").css("background-color","red");
			$("button#buttonRedhorse").css("color","white");
		});
	}
}

function openQtySmblight() {
	var spi = document.getElementById('qtySmblight');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtySmblight").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonSmblight").css("background-color","initial");
			$("button#buttonSmblight").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtySmblight").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonSmblight").css("background-color","red");
			$("button#buttonSmblight").css("color","white");
		});
	}
}

function openQtySmbflavored() {
	var spi = document.getElementById('qtySmbflavored');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtySmbflavored").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonSmbflavored").css("background-color","initial");
			$("button#buttonSmbflavored").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtySmbflavored").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonSmbflavored").css("background-color","red");
			$("button#buttonSmbflavored").css("color","white");
		});
	}
}

function openQtySmbpalepilsen() {
	var spi = document.getElementById('qtySmbpalepilsen');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtySmbpalepilsen").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonSmbpalepilsen").css("background-color","initial");
			$("button#buttonSmbpalepilsen").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtySmbpalepilsen").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonSmbpalepilsen").css("background-color","red");
			$("button#buttonSmbpalepilsen").css("color","white");
		});
	}
}

function openQtyCokecan() {
	var spi = document.getElementById('qtyCokecan');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyCokecan").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonCokecan").css("background-color","initial");
			$("button#buttonCokecan").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyCokecan").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonCokecan").css("background-color","red");
			$("button#buttonCokecan").css("color","white");
		});
	}
}

function openQtySpritecan() {
	var spi = document.getElementById('qtySpritecan');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtySpritecan").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonSpritecan").css("background-color","initial");
			$("button#buttonSpritecan").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtySpritecan").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonSpritecan").css("background-color","red");
			$("button#buttonSpritecan").css("color","white");
		});
	}
}

function openQtyBottleh2o() {
	var spi = document.getElementById('qtyBottleh2o');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyBottleh2o").slideUp(500);
			/*$("button#relayButtonB").text("Show");*/
			$("button#buttonBottleh2o").css("background-color","initial");
			$("button#buttonBottleh2o").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyBottleh2o").slideDown(500);
			//$("button#relayButtonB").text("Hide");
			$("button#buttonBottleh2o").css("background-color","red");
			$("button#buttonBottleh2o").css("color","white");
		});
	}
}

function openQtyIcecream() {
	var spi = document.getElementById('qtyIcecream');
	if (spi.style.display == 'block'){
		$(document).ready(function(){
			$("#qtyIcecream").slideUp(500);
			$("button#buttonIcecream").css("background-color","initial");
			$("button#buttonIcecream").css("color","#c82330");
		});
	} else {
		$(document).ready(function(){
			$("#qtyIcecream").slideDown(500);
			$("button#buttonIcecream").css("background-color","red");
			$("button#buttonIcecream").css("color","white");
		});
	}
}

function sentToChef() {
	alert("Your customer's order has been sent to the kitchen!");
}
