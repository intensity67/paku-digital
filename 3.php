<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Paku Order System</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>

<body id="paku_order_main">

	<?php 

		include 'constants.php';

		session_start();
  
		if(isset($_REQUEST['peopleNumber']))	
		
			$_SESSION['peopleNumber'] = $_REQUEST['peopleNumber']; 
		
		else if(!isset($_SESSION['peopleNumber']))

			header("Location: 2.php");

	?>


	<form action="process_order.php" method="POST">

		<div class="flavor">
			<h3>Flavor Guides:</h3>
		</div>
			<?php 
				if(isset($_SESSION['orders']['product_name'])){
  					include 'set_edited_index.php';
  				}
			?>		

<!-------------- Classic -------------->

		<fieldset>
			<legend>
			<?php 
				/*
				if(isset($_SESSION['orders']['product_name'])){
  					include 'set_edited_index.php';
  				}

  				* BTW I've moved this one up before the first <fieldset> tag (does that make a difference at all?) -> 
  				*/
			?>
				<div class="flavor-name">
					<label>
						<input value="1" type="checkbox"  class="" name="classic" onclick="openQtyClassic()" /><span>classic</span>
					</label>
				</div>
			</legend>

  			<div class="qty-selection" id="qtyClassic">
				<?php 
					$item_field_name = 'radioClassicqty'; 
					if(isset($classic_index) && isset($_SESSION['orders']['order_qty'][$classic_index]))
						$order_qty_opt   = $classic_qty; 
					else 
						unset($order_qty_opt);
						include 'order_radio_options/product_options.php'; 
				?>
				<div style="display:inline-block;">
					<label>
						<input type="radio" class="option-input radio" name="radioClassicqty" value=""/>Others
						<input  type="text" class="order_input_Others" name="othersClassicqty" value=""/>
					</label>
				</div>
			</div>

		</fieldset>


<!-------------- BBQ -------------->

		<fieldset>
			<legend>
				<div class="flavor-name">
					<label>
						<input value="1" type="checkbox" class="" name="bbq" onclick="openQtyBbq()" />bbq
					</label>
				</div>
			</legend>

  			<div class="qty-selection" id="qtyBbq">
  				
				<?php 

  					$item_field_name = 'radioBbqty'; 

 					if(isset($bbq_index) && isset($_SESSION['orders']['order_qty'][$bbq_index]))
  				 		$order_qty_opt   = $bbq_qty; 
  				 	else 
  				 		unset($order_qty_opt);

  				 	include 'order_radio_options/product_options.php'; 

  				  ?>
				<div style="display:inline-block;">

					<label>
					
						<input type="radio" class="option-input radio" name="radioBbqty" value=""/>Others
					
						<input  type="text" class="order_input_Others" name="othersradioBbqty" value=""/>
					
					</label>
  				
				</div>
	  		</div>
	  	</fieldset>


		<fieldset><legend>

			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="teriyaki" onclick="openQtyTeriyaki()" />teriyaki</label></div></legend>

  					<div class="qty-selection" id="qtyTeriyaki">
  						
						<?php 

  							$item_field_name = 'radioTeriyakiqty'; 

 							if(isset($teriyaki_index) && isset($_SESSION['orders']['order_qty'][$bbq_index]))
  						 		$order_qty_opt   = $teriyaki_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>


						 <label><input value="2" type="radio" class="option-input radio" name="radioTeriyakiqty" />Others</label>
						 <div><input  type="text" class="" name="othersradioTeriyakiqty" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	
		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="hotwings" onclick="openQtyHotwings()" />hot wings</label></div></legend>
  					<div class="qty-selection" id="qtyHotwings">
  				
						<?php 

  							$item_field_name = 'radioHotwingsqty'; 

 							if(isset($hotwings_index) && isset($_SESSION['orders']['order_qty'][$hotwings_index]))
  						 		$order_qty_opt   = $hotwings_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioHotwingsqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	

		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="spicy_vinegar" onclick="openQtySpicyvinegar()" />spicy vinegar</label></div></legend>
  					<div class="qty-selection" id="qtySpicyvinegar">

  							<?php 

  							$item_field_name = 'radioSpicyvinegar'; 

 							if(isset($hotwings_index) && isset($_SESSION['orders']['order_qty'][$hotwings_index]))
  						 		$order_qty_opt   = $hotwings_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioSpicyvinegarqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	
		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="lemon_pepper" onclick="openQtyLemonpepper()" />lemon pepper</label></div></legend>
  					<div class="qty-selection" id="qtyLemonpepper">
  		
  							<?php 

  							$item_field_name = 'radioLemonpepperqty'; 

 							if(isset($hotwings_index) && isset($_SESSION['orders']['order_qty'][$hotwings_index]))
  						 		$order_qty_opt   = $hotwings_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioLemonpepperqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	
		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="cream_cheese" onclick="openQtyCreamcheese()" />cream cheese</label></div></legend>
  					<div class="qty-selection" id="qtyCreamcheese">

  							<?php 

  							$item_field_name = 'radioCreamcheeseqty'; 

 							if(isset($creamCheese_index) && isset($_SESSION['orders']['order_qty'][$creamCheese_index]))
  						 		$order_qty_opt   = $creamCheese_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioCreamcheeseqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	


		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="honey_caramel" onclick="openQtyHoneycaramel()" />honey caramel</label></div></legend>
  					<div class="qty-selection" id="qtyHoneycaramel">
  	
  							<?php 

  							$item_field_name = 'radioCreamcheeseqty'; 

 							if(isset($honeyCarmel_index) && isset($_SESSION['orders']['order_qty'][$honeyCarmel_index]))
  						 		$order_qty_opt   = $honeyCarmel_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

  						  
						 <label><input value="2" type="radio" class="option-input radio" name="radioHoneycaramelqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>


	  	
		<fieldset><legend>	  		
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="mango_suave" onclick="openQtyMangosuave()" />mango suave</label></div></legend>
  					<div class="qty-selection" id="qtyMangosuave">

  							<?php 

  							$item_field_name = 'radioMangosuaveqty'; 

 							if(isset($mangoSuave_index) && isset($_SESSION['orders']['order_qty'][$mangoSuave_index]))
  						 		$order_qty_opt   = $mangoSuave_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioMangosuaveqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
		</div><!--end of .flavor-->



		<div class="unli-drinks">
			<h3>Unli Drinks @59:</h3>
				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="blue_lemonade" onclick="openQtyBluelemonade()" />Blue Lemonade</label></div></legend>
  					<div class="qty-selection" id="qtyBluelemonade">
  
  							<?php 

  							$item_field_name = 'radioBluelemonadeqty'; 

 							if(isset($blueLemonade_index) && isset($_SESSION['orders']['order_qty'][$blueLemonade_index]))
  						 		$order_qty_opt   = $blueLemonade_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioBluelemonadeqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>

			  		</div></fieldset>
				


				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="cucumber_lemonade" onclick="openQtyCucumberlemonade()" />Cucumber Lemonade</label></div></legend>
  					<div class="qty-selection" id="qtyCucumberlemonade">
  				
  							<?php 

  							$item_field_name = 'radioBluelemonadeqty'; 

 							if(isset($cucumberLemonade_index) && isset($_SESSION['orders']['order_qty'][$cucumberLemonade_index]))
  						 		$order_qty_opt   = $cucumberLemonade_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioCucumberlemonadeqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
			  		</div></fieldset>
							
		</div><!--end of .unli-drinks-->




		<div class="beer">
			<h3>Beer:</h3>
					<div class="bttn circular red bold centered">Bucket Promo
					<div style=""><small>(6 Beers) Mix and Match</small></div>
					</div>

			<fieldset>
			<legend>
				<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="red_horse" onclick="openQtyRedhorse()" />Red Horse</label></div>
			</legend>

  					<div class="qty-selection" id="qtyRedhorse">
  				
  							<?php 

  							$item_field_name = 'radioRedhorseqty'; 

 							if(isset($cucumberLemonade_index) && isset($_SESSION['orders']['order_qty'][$cucumberLemonade_index]))
  						 		$order_qty_opt   = $redHorse_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioRedhorseqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  		

	  		<fieldset><legend>

			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="smb_light" onclick="openQtySmblight()" />SMB Light</label></div></legend>

  					<div class="qty-selection" id="qtySmblight">

  						 <label><input value="2" type="radio" class="option-input radio" name="radioSmblightqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSmblightqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSmblightqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSmblightqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSmblightqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>		
			


			<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="smb_flavored" onclick="openQtySmbflavored()" />SMB Flavored</label></div></legend>
  					<div class="qty-selection" id="qtySmbflavored">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbflavoredqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSmbflavoredqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSmbflavoredqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSmbflavoredqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbflavoredqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>
			

			<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="smb_palepilsen" onclick="openQtySmbpalepilsen()" />SMB Pale Pilsen</label></div></legend>
  					<div class="qty-selection" id="qtySmbpalepilsen">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>	
	  				  			
		</div><!--end of .beer-->




		<div class="softdrinks-in-can">
			<h3>Softdrinks in can:</h3>
				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="coke" onclick="openQtyCokecan()" />Coke</label></div></legend>
  					<div class="qty-selection" id="qtyCokecan">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioCokeqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioCokeqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioCokeqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioCokeqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioCokeqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>
	  			


				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="sprite" onclick="openQtySpritecan()" />Sprite</label></div></legend>
  					<div class="qty-selection" id="qtySpritecan">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSpriteqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSpriteqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSpriteqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSpriteqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSpriteqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>
	  				
		  			
		</div><!--end of .softdrinks-in-can-->


		<div class="water">
			<h3>H<sub>2</sub>O:</h3>
			<fieldset><legend>
				<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="bottled_water" onclick="openQtyBottleh2o()" />Bottled Water</label></div></legend>
  					<div class="qty-selection" id="qtyBottleh2o">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioBottledwaterqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioBottledwaterqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioBottledwaterqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioBottledwaterqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioBottledwaterqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>
		  		</div></fieldset>
		

		<div class="dessert">
			<h3>Desserts:</h3>
			<fieldset><legend>
				
				<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="ice_cream" onclick="openQtyIcecream()" />Ice Cream</label></div></legend>
  					<div class="qty-selection" id="qtyIcecream">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioIcecreamqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioIcecreamqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioIcecreamqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioIcecreamqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioIcecreamqty" />Others</label>
						 <div><input  type="text" class="" name="others" /></div>

  						
  					</div>

		  		</div></fieldset>
		  
			
		</div><!--end of .dessert-->

		<button class="submit-form bttn circular red" name="send">Send to Kitchen</button>


		<input type="hidden" name="table_no" value="<?php echo $_POST['table_no']; ?>">
		
		<input type="hidden" name="no_of_people" value="<?php echo $_POST['no_of_people']; ?>">

	</form>


	<script src="js/jquery-3.2.1.js"></script>

    <script src="js/toggle.js"></script>

</body>
</html>
