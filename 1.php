<!DOCTYPE html>
<html>

<?php

require 'dbconnect.php';

?>

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Paku Order System</title>

<!-- 	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->

	<link rel="stylesheet" type="text/css" href="css/style.css">

	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script src="js/jquery-3.2.1.js"></script>

	<script type="text/javascript">
		
		$("document").ready(function(){
		
			$("#order_form").hide();
			
			$(".bttn").click(function(){

				$("#order_form").show();

				$("#no_of_people").focus();

				$("#table_no").val($(this).attr('value'));

			});

		});

	</script>
 
</head>


<body>

	<main class="app">

	  <div class="" style="margin-top: 50px;">

	  	<h2 style="text-align: center;">(Table number):</h2>
		<br>
			<center><a href="orders_history.php" class="btn btn-danger" > Go to Orders History </a></center>
		<br>
		<div class="tables five-cols">
			
			<?php include 'get_tables.php'; ?>

			<?php while($row = mysqli_fetch_assoc($tables_query)): ?>

				<div class="block centered">

 
							<a href="1_process.php?tableNumber=<?php echo $row['table_no']; ?>" 

								class="bttn circular red"> 

							<?php echo $row['table_no']; ?> 

		 					</a>


 		  
				</div>
			 
			 <?php endwhile; ?>

		</div>

	  </div>

	</main>

</body>

</html>
