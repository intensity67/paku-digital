<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Paku Order System</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
	<main class="app">
	  <div class="" style="margin-top: 50px;">
	  	<h2 style="text-align: center;">How many are you?</h2>
	  	<div class="tables three-cols">
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">1</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">2</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">3</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">4</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">5</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">6</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">7</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">8</a></div>
			<div class="block centered"><a href="order_form.html" class="bttn circular red-inverse">9</a></div>
		</div>
		<div style="text-align: center;">
			<form method="post" action="order_form.html">
				<label>Other - Please specify:</label><br>
				<input class="paku-forms" type="text" name="no_of_people"><br><br>
				<input type="submit" name="specifyNo_of_people" value="OK">
			</form>
		</div>
	  </div>
	</main>
	<?php 
		echo "".$peopleNumber;
	?>
	<script src="js/toggle.js"></script>
</body>
</html>
