<!DOCTYPE html>

<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Paku Order System</title>
<!-- 	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

</head>

<?php session_start(); ?> 

<body>

	<main class="app">

	  <div class="" style="margin-top: 50px;">

	  	<h2 style="text-align: center;">How many are you?</h2>

	  	<div class="tables three-cols">

	  		<?php for($i = 1; $i <= 9; $i++): ?>

					<div class="block centered"><a href="3.php?peopleNumber=<?php echo $i; ?>" class="bttn circular red"> <?php echo $i; ?> </a></div>
			
			<?php endfor; ?>

		</div>

		<div style="text-align: center;">
			<form method="post" action="order_form.html">
				<label>Other - Please specify:</label><br>
				<input class="paku-forms" type="text" name="no_of_people"><br><br>
				<input type="submit" name="specifyNo_of_people" value="OK" class="btn btn-success">
			</form>
		</div>

			<br>
			<center> <a href = "1.php" class="btn btn-info"> Back </a> </center>

	  </div>


	</main> 

	<script src="js/toggle.js"></script>

</body>

</html>

