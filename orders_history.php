<?php require 'dbconnect.php'; ?>

<?php include 'get_orders_hist_query.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
<div class="container">
	<header>
		<h3>Orders History</h3>
	</header>
	
	<main>

		<h4 style="text-decoration: underline;"> Current Orders </h4>
			<a href="1.php" class="btn btn-info">Back to Order</a>
		<div class="orders">

			<?php  while($row = mysqli_fetch_assoc($current_orders_query)){ ?>
				<div class="boxes">
					Order ID:&nbsp;<?php echo $row['order_id']; ?><br>
					<a href= "<?php echo 'view_order.php?order_id=' . $row['order_id']; ?>">
					Table No:&nbsp;<?php echo $row['table_no']; ?>
						</a><br>
					Date and Time In:<br>
						<?php echo date("M d, Y h:i A", strtotime($row['date_timein'])); ?><br>
				</div>
			<?php } ?>

		</div>

	<hr>

		<h4 style="text-decoration: underline;"> Billed Out </h4>

		<div class="orders">

			<?php  while($row = mysqli_fetch_assoc($orders_history_query)){ ?>
				<div class="boxes">
					Order ID:&nbsp;<?php echo $row['order_id']; ?><br>
					<a href= "<?php echo 'view_order.php?order_id='. $row['order_id']; ?>">
					Table No:&nbsp;<?php echo $row['table_no']; ?>
						</a><br>
					Date and Time In:<br>
						<?php echo date("M d, Y h:i A", strtotime($row['date_timein'])); ?><br>
					Date and Time Out:<br>
						<?php echo date("M d, Y h:i A", strtotime($row['date_timeout'])); ?><br>
				</div>
			<?php } ?>

		</div>
	</main>
	<footer>
		<a href="1.php" class="btn btn-info">Back to Order</a>
	</footer>
</div>





</body>
</html>
