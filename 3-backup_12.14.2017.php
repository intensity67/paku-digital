<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Paku Order System</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>

	<?php 

		session_start();
  
		if(isset($_REQUEST['peopleNumber']))	
		
			$_SESSION['peopleNumber'] = $_REQUEST['peopleNumber']; 
		
		else if(!isset($_SESSION['peopleNumber']))

			header("Location: 2.php");

	?>


	<form action="process_order.php" method="POST">

		<div class="flavor">
			<h3>Flavor Guides:</h3>
		</div>
		<fieldset><legend>


			<?php 


				if(isset($_SESSION['orders']['product_name'])){

  
					$classic_index = array_search('Classic', $_SESSION['orders']['product_name']); 
					
 					if(isset($_SESSION['orders']['order_qty'][$classic_index]))
						$classic_qty =  $_SESSION['orders']['order_qty'][$classic_index];
  
					$bbq_index = array_search('BBQ', $_SESSION['orders']['product_name']); 
					
					if(isset($_SESSION['orders']['order_qty'][$bbq_index]))
 						$bbq_qty =  $_SESSION['orders']['order_qty'][$bbq_index];
 

 				}
				
			?>


			<div class="flavor-name"><label><input value="1" type="checkbox"  class="" name="classic" onclick="openQtyClassic()" />classic</label></div></legend>



  					<div class="qty-selection" id="qtyClassic">

  						<?php 

  							$item_field_name = 'radioClassicqty'; 

 							if(isset($_SESSION['orders']['order_qty'][$classic_index]))
  						 		$order_qty_opt   = $classic_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 		include 'order_radio_options/product_options.php'; 

  						  ?>

						 <label><input value="2" type="radio" class="option-input radio" name="radioClassicqty" />Others</label>

						 <div><input  type="input" class="" name="others_radioClassicqty" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	


		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="bbq" onclick="openQtyBbq()" />bbq</label></div></legend>
  					<div class="qty-selection" id="qtyBbq">
  						
						<?php 

  							$item_field_name = 'radioClassicqty'; 

 							if(isset($_SESSION['orders']['order_qty'][$classic_index]))
  						 		$order_qty_opt   = $classic_qty; 
  						 	else 
  						 		unset($order_qty_opt);

  						 	include 'order_radio_options/product_options.php'; 

  						  ?>

  						  
  						 <label><input value="2" type="radio" class="option-input radio" name="radioBbqty" 

  						 	<?php if(isset($_SESSION['orders']['product_name']) && $bbq_qty == 2) echo 'checked'; ?> />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioBbqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioBbqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioBbqty" />8</label>

						 <label><input value="2" type="radio" class="option-input radio" name="radioBbqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>


		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="teriyaki" onclick="openQtyTeriyaki()" />teriyaki</label></div></legend>
  					<div class="qty-selection" id="qtyTeriyaki">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioTeriyakiqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioTeriyakiqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioTeriyakiqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioTeriyakiqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioTeriyakiqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	
		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="hotwings" onclick="openQtyHotwings()" />hot wings</label></div></legend>
  					<div class="qty-selection" id="qtyHotwings">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioHotwingsqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioHotwingsqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioHotwingsqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioHotwingsqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioHotwingsqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	

		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="spicy_vinegar" onclick="openQtySpicyvinegar()" />spicy vinegar</label></div></legend>
  					<div class="qty-selection" id="qtySpicyvinegar">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSpicyvinegarqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSpicyvinegarqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSpicyvinegarqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSpicyvinegarqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSpicyvinegarqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	
		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="lemon_pepper" onclick="openQtyLemonpepper()" />lemon pepper</label></div></legend>
  					<div class="qty-selection" id="qtyLemonpepper">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioLemonpepperqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioLemonpepperqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioLemonpepperqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioLemonpepperqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioLemonpepperqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	

	  	
		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="cream_cheese" onclick="openQtyCreamcheese()" />cream cheese</label></div></legend>
  					<div class="qty-selection" id="qtyCreamcheese">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioCreamcheeseqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioCreamcheeseqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioCreamcheeseqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioCreamcheeseqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioCreamcheeseqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  	


		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="honey_caramel" onclick="openQtyHoneycaramel()" />honey caramel</label></div></legend>
  					<div class="qty-selection" id="qtyHoneycaramel">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioHoneycaramelqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioHoneycaramelqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioHoneycaramelqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioHoneycaramelqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioHoneycaramelqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>


	  	
		<fieldset><legend>	  		
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="mango_suave" onclick="openQtyMangosuave()" />mango suave</label></div></legend>
  					<div class="qty-selection" id="qtyMangosuave">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioMangosuaveqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioMangosuaveqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioMangosuaveqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioMangosuaveqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioMangosuaveqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
		</div><!--end of .flavor-->



		<div class="unli-drinks">
			<h3>Unli Drinks @59:</h3>
				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="blue_lemonade" onclick="openQtyBluelemonade()" />Blue Lemonade</label></div></legend>
  					<div class="qty-selection" id="qtyBluelemonade">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioBluelemonadeqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioBluelemonadeqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioBluelemonadeqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioBluelemonadeqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioBluelemonadeqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>

			  		</div></fieldset>
				


				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="cucumber_lemonade" onclick="openQtyCucumberlemonade()" />Cucumber Lemonade</label></div></legend>
  					<div class="qty-selection" id="qtyCucumberlemonade">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioCucumberlemonadeqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioCucumberlemonadeqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioCucumberlemonadeqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioCucumberlemonadeqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioCucumberlemonadeqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
			  		</div></fieldset>
							
		</div><!--end of .unli-drinks-->




		<div class="beer">
			<h3>Beer:</h3>
					<div class="bttn circular red bold centered">Bucket Promo
					<div style=""><small>(6 Beers) Mix and Match</small></div>
					</div>

			<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="red_horse" onclick="openQtyRedhorse()" />Red Horse</label></div></legend>
  					<div class="qty-selection" id="qtyRedhorse">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioRedhorseqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioRedhorseqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioRedhorseqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioRedhorseqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioRedhorseqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>
	  		

	  		<fieldset><legend>
			<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="smb_light" onclick="openQtySmblight()" />SMB Light</label></div></legend>
  					<div class="qty-selection" id="qtySmblight">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSmblightqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSmblightqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSmblightqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSmblightqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSmblightqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  		</div></fieldset>		
			


			<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="smb_flavored" onclick="openQtySmbflavored()" />SMB Flavored</label></div></legend>
  					<div class="qty-selection" id="qtySmbflavored">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbflavoredqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSmbflavoredqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSmbflavoredqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSmbflavoredqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbflavoredqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>
			

			<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="smb_palepilsen" onclick="openQtySmbpalepilsen()" />SMB Pale Pilsen</label></div></legend>
  					<div class="qty-selection" id="qtySmbpalepilsen">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSmbpalepilsenqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>	
	  				  			
		</div><!--end of .beer-->




		<div class="softdrinks-in-can">
			<h3>Softdrinks in can:</h3>
				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="coke" onclick="openQtyCokecan()" />Coke</label></div></legend>
  					<div class="qty-selection" id="qtyCokecan">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioCokeqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioCokeqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioCokeqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioCokeqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioCokeqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>
	  			


				<fieldset><legend>
					<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="sprite" onclick="openQtySpritecan()" />Sprite</label></div></legend>
  					<div class="qty-selection" id="qtySpritecan">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioSpriteqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioSpriteqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioSpriteqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioSpriteqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioSpriteqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
	  				</div></fieldset>
	  				
		  			
		</div><!--end of .softdrinks-in-can-->


		<div class="water">
			<h3>H<sub>2</sub>O:</h3>
			<fieldset><legend>
				<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="bottled_water" onclick="openQtyBottleh2o()" />Bottled Water</label></div></legend>
  					<div class="qty-selection" id="qtyBottleh2o">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioBottledwaterqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioBottledwaterqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioBottledwaterqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioBottledwaterqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioBottledwaterqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>
		  		</div></fieldset>
		

		<div class="dessert">
			<h3>Desserts:</h3>
			<fieldset><legend>
				
				<div class="flavor-name"><label><input value="1" type="checkbox" class="" name="ice_cream" onclick="openQtyIcecream()" />Ice Cream</label></div></legend>
  					<div class="qty-selection" id="qtyIcecream">
  						 <label><input value="2" type="radio" class="option-input radio" name="radioIcecreamqty" />2</label>
  						 <label><input value="4" type="radio" class="option-input radio" name="radioIcecreamqty" />4</label>
  					     <label><input value="6" type="radio" class="option-input radio" name="radioIcecreamqty" />6</label>
						 <label><input value="8" type="radio" class="option-input radio" name="radioIcecreamqty" />8</label>
						 <label><input value="2" type="radio" class="option-input radio" name="radioIcecreamqty" />Others</label>
						 <div><input  type="input" class="" name="others" /></div>

  						
  					</div>

		  		</div></fieldset>
		  
			
		</div><!--end of .dessert-->

		<button class="submit-form" name="send">Send to Kitchen</button>


		<input type="hidden" name="table_no" value="<?php echo $_POST['table_no']; ?>">
		
		<input type="hidden" name="no_of_people" value="<?php echo $_POST['no_of_people']; ?>">

	</form>


	<script src="js/jquery-3.2.1.js"></script>

    <script src="js/toggle.js"></script>

</body>
</html>
