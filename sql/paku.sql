-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2018 at 01:06 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paku`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(1, 'Flavor Guides'),
(2, 'Unli Drinks @59:'),
(3, 'Beer'),
(4, 'Softdrinks in can'),
(5, 'H2O'),
(6, 'Desserts');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `table_no` int(11) NOT NULL,
  `no_of_people` int(11) NOT NULL,
  `date_timein` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_timeout` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `table_no`, `no_of_people`, `date_timein`, `date_timeout`, `status`) VALUES
(1, 5, 0, '2017-12-01 04:48:27', '2017-12-13 13:45:50', 1),
(8, 5, 2, '2017-12-12 05:31:59', '2017-12-13 13:45:50', 1),
(9, 5, 2, '2017-12-13 16:11:45', '2017-12-13 16:11:45', 1),
(10, 7, 0, '2017-12-13 13:57:20', '2017-12-13 13:57:20', 0),
(15, 2, 2, '2017-12-13 17:18:26', '2017-12-13 17:18:26', 1),
(16, 2, 5, '2017-12-13 17:18:26', '2017-12-13 17:18:26', 1),
(17, 2, 5, '2017-12-13 17:18:26', '2017-12-13 17:18:26', 1),
(18, 2, 5, '2017-12-13 18:06:48', '2017-12-13 18:06:48', 1),
(19, 5, 5, '2017-12-13 18:07:27', NULL, 0),
(20, 8, 5, '2017-12-13 18:15:04', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `detail_id` int(11) NOT NULL,
  `fk_order_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`detail_id`, `fk_order_id`, `qty`, `description`) VALUES
(2, 1, 6, 'Red Horse'),
(3, 1, 9, 'hot wings'),
(4, 1, 5, 'coke'),
(5, 1, 7, 'coffee'),
(18, 8, 6, 'Blue Lemonade'),
(21, 10, 6, 'Hotwings'),
(29, 15, 6, 'Hotwings'),
(30, 16, 4, 'Classic'),
(31, 17, 4, 'Mango Suave'),
(32, 18, 4, 'Classic'),
(33, 19, 6, 'Lemon Pepper'),
(34, 19, 6, 'Honey Caramel'),
(35, 20, 6, 'Cream Cheese'),
(36, 20, 8, 'Spicy Vinegar'),
(37, 20, 4, 'Classic'),
(38, 20, 4, 'BBQ'),
(39, 20, 4, 'Teriyaki'),
(40, 20, 6, 'Hotwings'),
(41, 20, 6, 'Lemon Pepper'),
(42, 20, 6, 'Cream Cheese'),
(43, 20, 0, 'Red Horse'),
(44, 20, 6, 'SMB Light'),
(45, 20, 6, 'SMB Flavored'),
(46, 20, 2, 'Smb Palepilsen'),
(47, 20, 4, 'Coke'),
(48, 20, 6, 'Sprite'),
(49, 20, 4, 'Bottled Water'),
(50, 20, 6, 'Ice cream');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` int(11) NOT NULL,
  `prod_cat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_price`, `prod_cat_id`) VALUES
(1, 'classic', 0, 1),
(2, 'bbq', 0, 1),
(3, 'teriyaki', 0, 1),
(4, 'hot wings', 0, 1),
(5, 'spicy vinegar', 0, 1),
(6, 'lemon pepper', 0, 1),
(7, 'cream cheese', 0, 1),
(8, 'honey caramel', 0, 1),
(9, 'mango suave', 0, 1),
(10, 'Blue Lemonade', 0, 2),
(11, 'Cucumber Lemonade', 0, 2),
(12, 'SMB Light', 0, 3),
(13, 'SMB Flavored', 0, 3),
(14, 'SMB Pale Pilsen', 0, 3),
(15, 'Coke', 0, 4),
(16, 'Sprite', 0, 4),
(17, 'Bottled Water', 0, 5),
(18, 'Ice Cream', 0, 6),
(19, 'Red Horse', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `table_id` int(11) NOT NULL,
  `table_no` int(11) NOT NULL,
  `no_people` int(11) NOT NULL,
  `table_status` enum('occupied','vacant') NOT NULL DEFAULT 'vacant'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`table_id`, `table_no`, `no_people`, `table_status`) VALUES
(1, 1, 4, 'occupied'),
(2, 2, 0, 'vacant'),
(3, 3, 0, 'vacant'),
(4, 4, 0, 'vacant'),
(5, 5, 5, 'occupied'),
(6, 6, 0, 'vacant'),
(7, 7, 0, 'vacant'),
(8, 8, 4, 'occupied'),
(9, 9, 0, 'vacant'),
(10, 10, 0, 'vacant'),
(11, 11, 0, 'vacant'),
(12, 12, 0, 'vacant'),
(13, 13, 0, 'vacant'),
(14, 14, 0, 'vacant'),
(15, 15, 0, 'vacant'),
(16, 16, 0, 'vacant'),
(17, 17, 0, 'vacant'),
(18, 18, 0, 'vacant'),
(19, 19, 0, 'vacant'),
(20, 20, 0, 'vacant'),
(21, 21, 0, 'vacant'),
(22, 22, 0, 'vacant'),
(23, 23, 0, 'vacant'),
(24, 24, 0, 'vacant'),
(25, 25, 0, 'vacant'),
(26, 26, 0, 'vacant'),
(27, 27, 0, 'vacant'),
(28, 28, 0, 'vacant'),
(29, 29, 0, 'vacant'),
(30, 30, 0, 'vacant'),
(31, 31, 0, 'vacant'),
(32, 32, 0, 'vacant'),
(33, 33, 0, 'vacant'),
(34, 34, 0, 'vacant'),
(35, 35, 0, 'vacant'),
(36, 36, 0, 'vacant'),
(37, 37, 0, 'vacant'),
(38, 38, 0, 'vacant'),
(39, 39, 0, 'vacant'),
(40, 40, 0, 'vacant'),
(41, 41, 0, 'vacant'),
(42, 42, 0, 'vacant'),
(43, 43, 0, 'vacant'),
(44, 44, 0, 'vacant'),
(45, 45, 0, 'vacant'),
(46, 46, 0, 'vacant'),
(47, 47, 0, 'vacant'),
(48, 48, 0, 'vacant'),
(49, 49, 0, 'vacant'),
(50, 50, 0, 'vacant'),
(51, 51, 0, 'vacant'),
(52, 52, 0, 'vacant'),
(53, 53, 0, 'vacant'),
(54, 54, 0, 'vacant'),
(55, 55, 0, 'vacant'),
(56, 56, 0, 'vacant'),
(57, 57, 0, 'vacant'),
(58, 58, 0, 'vacant'),
(59, 59, 0, 'vacant'),
(60, 60, 0, 'vacant');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`detail_id`),
  ADD KEY `FK_Order_Id` (`fk_order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`table_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK_Order_Id` FOREIGN KEY (`fk_order_id`) REFERENCES `orders` (`order_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
