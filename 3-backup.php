<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Paku Order System</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">

	<script src="js/jquery-3.2.1.js"></script>

	<script type="text/javascript">
		
		$("document").ready(function(){
		
			$("#order_form").hide();
			
			$(".bttn").click(function(){

				$("#order_form").show();

				$("#no_of_people").focus();

				$("#table_no").val($(this).attr('value'));

			});

		});

	</script>

</head>

<body>

	<main class="app">
	  <div class="" style="margin-top: 50px;">

	  	<h2 style="text-align: center;">How many are you?</h2>

	  	<?php include 'get_tables.php'; ?>

	  	<div class="tables three-cols">

	  		<?php while($row = mysqli_fetch_assoc($tables_query)): ?>

				<div class="block centered">

					<a href="#" class="bttn circular red-inverse" value = "<?php echo $row['table_no']; ?>" > 

					<?php echo $row['table_no']; ?> 
					
					</a>

				</div> 
 			
 			<?php endwhile; ?>

			<div style="text-align: center; " id = "order_form" >

				<form method = "POST" action = "3_order_form.php">

					<label>Other - Please specify:</label><br>

					<input class="paku-forms" type="text" id= "no_of_people" name="no_of_people">

					<br><br>

					<input type="submit" name="specifyNo_of_people" value="OK">

					<input type="hidden" id= "table_no" name="table_no">

				</form>

			</div>

	  </div>


	</main>


	<script src="js/toggle.js"></script>


</body>
</html>
