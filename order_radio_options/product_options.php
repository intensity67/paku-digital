 
<?php for($opt_cnt = ORDER_OPTION_INIT; $opt_cnt <= ORDER_OPTION_LIMIT; $opt_cnt += ORDER_OPTION_INCREMENT): ?>

	<div style="display:inline-block;">

		 <label>

			 <input type="radio" class="option-input radio" 

			 name="<?php echo $item_field_name; ?>"  
			 
			 value="<?php echo $opt_cnt; ?>" 

			 <?php if(isset($_SESSION['orders']['product_name']) && isset($order_qty_opt) && $order_qty_opt == $opt_cnt) 

			 		echo 'checked'; 
			 ?> />

			 <?php echo $opt_cnt; ?>

		</label>

	</div>

<?php endfor; ?>
