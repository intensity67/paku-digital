<?php 

require 'dbconnect.php';

require 'values_get_orders_qty.php';


if(isset($_POST['classic'])){


$orders['product_name'][] 	= "Classic";
$orders['order_qty'][] 		= $radioClassicqty;

} 

if(isset($_POST['bbq']) && !empty($_POST['bbq'])){

	
$orders['product_name'][] 	= "BBQ";
$orders['order_qty'][] 		= $radioBbqty;

}

if(isset($_POST['teriyaki'])){

	
$orders['product_name'][] 	= "Teriyaki";
$orders['order_qty'][] 		= $radioTeriyakiqty;

}

if(isset($_POST['hotwings'])){

	
$orders['product_name'][] 	= "Hotwings";
$orders['order_qty'][] 		= $radioHotwingsqty;

}

if(isset($_POST['spicy_vinegar'])){

	
$orders['product_name'][] 	= "Spicy Vinegar";
$orders['order_qty'][] 		= $radioSpicyvinegarqty;

}

if(isset($_POST['lemon_pepper'])){

	
$orders['product_name'][] 	= "Lemon Pepper";
$orders['order_qty'][] 		= $radioLemonpepperqty;

}

if(isset($_POST['cream_cheese'])){

	
$orders['product_name'][] 	= "Cream Cheese";
$orders['order_qty'][] 		= $radioCreamcheeseqty;

}

if(isset($_POST['honey_caramel'])){

	
$orders['product_name'][] 	= "Honey Caramel";
$orders['order_qty'][] 		= $radioHoneycaramelqty;

}

if(isset($_POST['mango_suave'])){

	
$orders['product_name'][] 	= "Mango Suave";
$orders['order_qty'][] 		= $radioMangosuaveqty;

}

if(isset($_POST['blue_lemonade'])){

	
$orders['product_name'][] 	= "Blue Lemonade";
$orders['order_qty'][] 		= $radioBluelemonadeqty;

}

if(isset($_POST['cucumber_lemonade'])){

	
$orders['product_name'][] 	= "Cucumber Lemonade";
$orders['order_qty'][] 		= $radioCucumberlemonadeqty;

}


if(isset($_POST['red_horse'])){

	
$orders['product_name'][] 	= "Red Horse";
$orders['order_qty'][] 		= $radioRedhorseqty;

}



if(isset($_POST['smb_light'])){

	
$orders['product_name'][] 	= "SMB Light";
$orders['order_qty'][] 		= $radioSmblightqty;

}



if(isset($_POST['smb_flavored'])){

	
$orders['product_name'][] 	= "SMB Flavored";
$orders['order_qty'][] 		= $radioSmbflavoredqty;

}


if(isset($_POST['smb_palepilsen'])){

	
$orders['product_name'][] 	= "Smb Palepilsen";
$orders['order_qty'][] 		= $radioSmbpalepilsenqty;

}

if(isset($_POST['coke'])){

	
$orders['product_name'][] 	= "Coke";
$orders['order_qty'][] 		= $radioCokeqty;

}

if(isset($_POST['sprite'])){

	
$orders['product_name'][] 	= "Sprite";
$orders['order_qty'][] 		= $radioSpriteqty;

}


if(isset($_POST['bottled_water'])){

	
$orders['product_name'][] 	= "Bottled Water";
$orders['order_qty'][] 		= $radioBottledwaterqty;

}



if(isset($_POST['ice_cream'])){

	
$orders['product_name'][] 	= "Ice cream";
$orders['order_qty'][] 		= $radioIcecreamqty;

} 


session_start();

$_SESSION['orders'] 		= $orders; 

header("Location: finish_order.php");

?>