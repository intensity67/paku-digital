<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Paku Order System</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
</head>
<body>
	<main class="app">
	  <div class="" style="margin-top: 50px;">
	  	<h2 style="text-align: center;">May I have your order?</h2>
	  	<div class="container">
	  		<h3>Flavor Guide:</h3>

	  			<div class="flavor">
	  				<div class="flavor-name"><button id="buttonClassic" onclick="openQtyClassic()" class="bttn circular red" >classic</button></div>
  					<div class="qty-selection" id="qtyClassic">
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
	  			</div>
	  			<div class="flavor">
					<div class="flavor-name"><button id="buttonBbq" onclick="openQtyBbq()" class="bttn circular red" >bbq</button></div>
					<div class="qty-selection" id="qtyBbq">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
	  			</div>
				<div class="flavor">
					<div class="flavor-name"><button id="buttonTeriyaki" onclick="openQtyTeriyaki()" class="bttn circular red" >teriyaki</button></div>
					<div class="qty-selection" id="qtyTeriyaki">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
	  			</div>
	  			<div class="flavor">
					<div class="flavor-name"><button id="buttonHotwings" onclick="openQtyHotwings()" class="bttn circular red" >hot wings</button></div>
					<div class="qty-selection" id="qtyHotwings">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
	  			</div>
	  			<div class="flavor">
					<div class="flavor-name"><button id="buttonSpicyvinegar" onclick="openQtySpicyvinegar()" class="bttn circular red" >spicy vinegar</button></div>
					<div class="qty-selection" id="qtySpicyvinegar">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
				</div>
				<div class="flavor">
					<div class="flavor-name"><button id="buttonLemonpepper" onclick="openQtyLemonpepper()" class="bttn circular red" >lemon pepper</button></div>
					<div class="qty-selection" id="qtyLemonpepper">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
				</div>
				<div class="flavor">
					<div class="flavor-name"><button id="buttonCreamcheese" onclick="openQtyCreamcheese()" class="bttn circular red" >cream cheese</button></div>
					<div class="qty-selection" id="qtyCreamcheese">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
				</div>
				<div class="flavor">
					<div class="flavor-name"><button id="buttonHoneycaramel" onclick="openQtyHoneycaramel()" class="bttn circular red" >honey caramel</button></div>
					<div class="qty-selection" id="qtyHoneycaramel">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
				</div>
				<div class="flavor">
					<div class="flavor-name"><button id="buttonMangosuave" onclick="openQtyMangosuave()" class="bttn circular red" >mango suave</button></div>
					<div class="qty-selection" id="qtyMangosuave">
						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="no_of_people"><br><br>
  					</div>
				</div>

	  		<h3>DRINKS</h3>
	  			<div class="unli-drinks">
	  				<h4>UNLI-DRINKS @ 59</h4>
	  				<div class="drink"><button id="buttonBluelemonade" onclick="openQtyBluelemonade()" class="bttn circular red" >Blue Lemonade</button></div>
					<div class="qty-selection" id="qtyBluelemonade">
  						<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="qtyNo_of_Bluelemonade"><br>
  					</div>
  					<div class="drink"><button id="buttonCucumberlemonade" onclick="openQtyCucumberlemonade()" class="bttn circular red" >Cucumber Lemonade</button></div>
					<div class="qty-selection" id="qtyCucumberlemonade">
  						<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<label>Others: </label><br>
						<input class="paku-forms" type="text" name="qtyNo_of_Cucumberlemonade"><br>
  					</div>
  				</div>

  				<div class="beer">
	  				<h4>BEER</h4>
	  				<div class="drink"><button class="bttn circular red bold">Bucket Promo</button>
	  				<div style="display: inline-block;"><small style="text-align: center;">(6 Beers) Mix and Match</small></div>
	  				</div>

	  				<div class="drink"><button id="buttonRedhorse" onclick="openQtyRedhorse()" class="bttn circular red" >Red Horse</button></div>
					<div class="qty-selection" id="qtyRedhorse">
	  					<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<a class="bttn circular red" value="6">6</a>
	  				</div>
	  				<div class="drink"><button id="buttonSmblight" onclick="openQtySmblight()" class="bttn circular red" >SMB Light</button></div>
					<div class="qty-selection" id="qtySmblight">
	  					<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<a class="bttn circular red" value="6">6</a>
	  				</div>
	  				<div class="drink"><button id="buttonSmbflavored" onclick="openQtySmbflavored()" class="bttn circular red" >SMB Flavored</button></div>
					<div class="qty-selection" id="qtySmbflavored">
	  					<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<a class="bttn circular red" value="6">6</a>
	  				</div>
	  				<div class="drink"><button id="buttonSmbpalepilsen" onclick="openQtySmbpalepilsen()" class="bttn circular red" >SMB Pale Pilsen</button></div>
					<div class="qty-selection" id="qtySmbpalepilsen">
	  					<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<a class="bttn circular red" value="6">6</a>
	  				</div>
	  			</div>
  				
  				<div class="beer">
	  				<h4>Softdrinks in Can</h4>
	  				<div class="drink"><button id="buttonCokecan" 
	  				onclick="openQtyCokecan()" class="bttn circular red" 
	  				>Coke</button></div>
					<div class="qty-selection" id="qtyCokecan">
	  					<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<div><label>Others: </label></div>
						<input class="paku-forms" type="text" name=""><br><br>
	  				</div>
	  				<div class="drink"><button id="buttonSpritecan" 
	  				onclick="openQtySpritecan()" class="bttn circular red" 
	  				>Sprite</button></div>
					<div class="qty-selection" id="qtySpritecan">
	  					<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="4">4</a>
  						<a class="bttn circular red" value="6">6</a>
						<a class="bttn circular red" value="8">8</a>
						<div><label>Others: </label></div>
						<input class="paku-forms" type="text" name=""><br><br>
	  				</div>

	  				<h4>H<sub>2</sub>O</h4>
	  				<div class="drink"><button id="buttonBottleh2o" 
	  				onclick="openQtyBottleh2o()" class="bttn circular red" 
	  				>Bottled Water</button></div>
					<div class="qty-selection" id="qtyBottleh2o">
	  					<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<a class="bttn circular red" value="6">6</a>
	  				</div>
	  			</div>
	  		<h3>Desserts</h3>
	  			<div class="desserts">
	  				<h4>Ice Cream</h4>
	  				<!-- <div class="drink"><a class="bttn circular red" href="#">Blue Lemonade</a></div> -->
	  				<div class="qty-selection" style="display: block;">
  						<a class="bttn circular red" value="1">1</a>
  						<a class="bttn circular red" value="2">2</a>
  						<a class="bttn circular red" value="3">3</a>
						<a class="bttn circular red" value="4">4</a>
						<a class="bttn circular red" value="5">5</a>
						<input class="paku-forms" type="text" name="no_of_ice_creams"><br><br>
  					</div>  					
  				</div>
				<!-- <input type="submit" name="send_to_kitchen" value="Send to Kitchen"> -->
				<a href="index.html" class="bttn circular red" onclick="sentToChef()">Send to Kitchen!</a>
	  	</div>
	  </div>
	</main>
	
	<script src="js/jquery-3.2.1.js"></script>
    <script src="js/toggle.js"></script>
</body>
</html>
